fun containsEven(collection: Collection<Int>): Boolean =
        collection.any {check: Int -> check % 2 == 0}
